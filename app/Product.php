<?php

namespace App;

use App\Scopes\AvailableScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use softDeletes,
        Imageable,
        Noteable;

    protected $fillable = [
        'name',
        'description',
        'type_id',
        'is_available',
        'image'
    ];

    protected $casts = [
        'is_available' => 'boolean',

    ];

    protected $appends = ['is_favorited'];
    protected $with = ['ProductType', 'CurrentPrice', 'CurrentSalePrice'];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new AvailableScope);
    }

    public function ProductType()
    {
        return $this->hasOne(ProductType::class, 'id', 'type_id');
    }

    public function Prices()
    {
        return $this->hasMany(Price::class);
    }

    public function CurrentPrice()
    {
        return $this->hasOne(Price::class)
            ->where('is_current', '=', true)
            ->where('is_sale', '=', false);
    }

    public function CurrentSalePrice()
    {
        return $this->hasOne(Price::class)
            ->where('is_current', '=', true)
            ->where('is_sale', '=', true);
    }

    /**
     * A reply can be favorited.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Favorites()
    {
        return $this->hasMany(Favorite::class);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOneThrough
     */
    public function User()
    {
        return $this->hasOneThrough(User::class, Favorite::class);
    }

    /**
     * Favorite the current reply.
     *
     * @return Model
     */
    public function favorite()
    {
        $attributes = ['user_id' => request()->user()->id];
        if (! $this->favorites()->where($attributes)->exists()) {
            return $this->favorites()->create($attributes);
        }
    }

    /**
     * Unfavorite the current reply.
     */
    public function unfavorite()
    {
        $attributes = ['user_id' => request()->user()->id];
        $this->favorites()->where($attributes)->get()->each->delete();
    }
    /**
     * Determine if the current reply has been favorited.
     *
     * @return bool
     */
    public function getIsFavoritedAttribute()
    {
        if (request()->user()) {
            return (bool) $this->Favorites()->where('user_id', '=', request()->user()->id)->exists();
        }
        return false;
    }
}
