<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use SoftDeletes,
        HasApiTokens,
        Notifiable,
        Imageable,
        Noteable,
        Recordable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'email_verified_at',
        'confirmation_token',
        'confirmation_token_expiry',
        'is_admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'confirmation_token',
        'confirmation_token_expiry',
        'deleted_at',
        'is_admin'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     *
     * @var array
     */
    protected $dates = [
        'confirmation_token_expiry'
    ];

    public function Favorites()
    {
        return $this->hasMany(Favorite::class);
    }

    public function FavoritedProducts()
    {
        return $this->belongsToMany(
            Product::class,
            Favorite::class,
            'user_id',
            'product_id',
            'id',
            'id');
    }

    public function Addresses()
    {
        return $this->hasMany(Address::class);
    }

    public function Phones()
    {
        return $this->hasMany(PhoneNumber::class);
    }

    public function Avatar()
    {
        return $this->morphOne(Image::class, 'imageable')
            ->where('is_main', '=', true)
            ->latest();
    }

    /**
     * Confirm token is not expired
     */
    public function setConfirmationToken(): void
    {
        $this->confirmation_token = substr(md5(now()), 0, 25);
        $this->confirmation_token_expiry = Carbon::now()->addDays(3)->toDateString();
        $this->save();
    }

    /**
     * Confirm token is not expired
     */
    public function confirmationTokenHasExpired()
    {
        $nowDate = Carbon::now();
        $tokenDate = new Carbon($this->confirmation_token_expiry);

        return $nowDate->isAfter($tokenDate);
    }

    /**
     * Mark the user's account as confirmed.
     */
    public function confirm()
    {
        $this->email_verified_at = Carbon::now();
        $this->confirmation_token = null;
        $this->confirmation_token_expiry = null;

        $this->save();

        return true;
    }

    /**
     * Mark the user's confirmation token expired.
     */
    public function expireToken()
    {
        $this->confirmation_token_expiry = Carbon::now()->subDays(5);
        $this->save();
    }

    public function getStorageDirectoryAttribute()
    {
        return substr(md5($this->created_at), 0, 25);
    }
}
