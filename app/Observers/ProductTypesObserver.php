<?php

namespace App\Observers;

use App\Product;
use App\ProductType;
use App\Scopes\AvailableScope;

class ProductTypesObserver
{
    /**
     * Handle the product type "created" event.
     *
     * @param  \App\ProductType  $productType
     * @return void
     */
    public function created(ProductType $productType)
    {
        //
    }

    /**
     * Handle the product type "updated" event.
     *
     * @param  \App\ProductType  $productType
     * @return void
     */
    public function updated(ProductType $productType)
    {
        Product::withoutGlobalScope(AvailableScope::class)->where('type_id', $productType->id)
            ->update(['is_available' => $productType->is_available]);
    }

    /**
     * Handle the product type "deleted" event.
     *
     * @param  \App\ProductType  $productType
     * @return void
     */
    public function deleted(ProductType $productType)
    {
        //
    }

    /**
     * Handle the product type "restored" event.
     *
     * @param  \App\ProductType  $productType
     * @return void
     */
    public function restored(ProductType $productType)
    {
        //
    }

    /**
     * Handle the product type "force deleted" event.
     *
     * @param  \App\ProductType  $productType
     * @return void
     */
    public function forceDeleted(ProductType $productType)
    {
        //
    }
}
