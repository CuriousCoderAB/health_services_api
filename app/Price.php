<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Price extends Model
{
    use softDeletes,
        Noteable;

    protected $fillable = [
        'price',
        'product_id',
        'is_current',
        'is_sale'
    ];

    protected $casts = [
        'is_current' => 'boolean',
        'is_sale' => 'boolean'
    ];

    public function Product()
    {
        return $this->belongsTo(Product::class);
    }
}
