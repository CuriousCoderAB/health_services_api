<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{
    use SoftDeletes,
        Noteable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'street_address',
        'unit_information',
        'city',
        'province',
        'country',
        'postal_code'
    ];

    public function User () {
        return $this->belongsTo(User::class);
    }
}
