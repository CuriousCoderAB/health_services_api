<?php

namespace App\Http\Controllers\Products;

use App\Http\Controllers\Controller;
use App\Image;
use App\Product;

class ImagesController extends Controller
{
    public function __construct()
    {
        $this->middleware(['admin'])->except(['show']);
        $this->middleware(['auth:api', 'mustVerify']);
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $images = Image::all();
        return response()->json(['images' => $images], 200);
    }


    /**
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Product $product)
    {
        request()->validate([
            'image' => ['required', 'image', 'mimes:jpg,jpeg,bmp,png', 'max:2068'],
            'caption' => ['nullable', 'string', 'max:255'],
            'reference' => ['nullable', 'string', 'max:255'],
            'description' => ['nullable', 'string', 'max:255'],
            'is_main' => ['nullable', 'boolean']
        ]);

        $meta = [
            'caption' => request()->caption,
            'reference' => request()->reference,
            'description' => request()->description,
            'main' => request()->is_main
        ];

        $product->storeImage(request()->file('image'), $meta, true);

        return response()->json('Image added.', 201);
    }


    /**
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Product $product)
    {
        $images = $product->images();
        return response()->json(['images' => $images], 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        Image::destroy($id);
        return response()->json('Image deleted', 200);
    }
}
