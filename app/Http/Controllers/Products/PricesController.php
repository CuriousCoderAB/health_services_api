<?php

namespace App\Http\Controllers\Products;

use App\Http\Controllers\Controller;
use App\Price;
use Illuminate\Http\Request;

class PricesController extends Controller
{
    public function __construct()
    {
        $this->middleware(['admin'])->except(['index', 'show']);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $prices = Price::all();
        return response()->json(['prices' => $prices], 200);
    }

    /**
     * @param Request $request
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $price = Price::where('id', $id)->get();
        return response()->json(['price' => $price], 200);
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * @param $id
     */
    public function destroy($id)
    {
        //
    }
}
