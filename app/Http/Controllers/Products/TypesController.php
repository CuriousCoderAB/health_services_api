<?php

namespace App\Http\Controllers\Products;

use App\Http\Controllers\Controller;
use App\Scopes\AvailableScope;
use App\ProductType;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class TypesController extends Controller
{
    /**
     * TypesController constructor.
     */
    public function __construct()
    {
        $this->middleware(['admin'])->except(['index', 'show']);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $product_types = ProductType::all();
        return response()->json(['product_types' => $product_types], 200);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'unique:product_types,name', 'min:2', 'max:50'],
            'description' => ['required', 'min:2', 'max:250'],
            'is_available' => ['required', 'boolean']
        ]);

        ProductType::create($request->all());

        return response()->json(['message', 'Product type created!'], 201);
    }


    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $product_type = ProductType::where('id', $id)->get();
        return response()->json(['product_type' => $product_type], 200);
    }


    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', Rule::unique('product_types')->ignore($id), 'min:2', 'max:50'],
            'description' => ['required', 'min:2', 'max:250'],
            'is_available' => ['required', 'boolean']
        ]);

        ProductType::withoutGlobalScope(AvailableScope::class)->find($id)->update($request->all());
        return response()->json(['message', 'Product type updated!'], 202);
    }


    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        ProductType::destroy($id);
        return response()->json(['message' => 'Product type deleted!'], 202);
    }
}
