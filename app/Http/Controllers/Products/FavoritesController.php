<?php

namespace App\Http\Controllers\Products;

use App\Http\Controllers\Controller;
use App\Product;

class FavoritesController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:api', 'mustVerify']);
    }

    /**
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Product $product)
    {
        $product->favorite();
        return response()->json(['message' => 'The product was added to favorites.'], 201);
    }


    /**
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Product $product)
    {
        $product->unfavorite();
        return response()->json(['message' => 'The product was removed from favorites.'], 202);
    }
}
