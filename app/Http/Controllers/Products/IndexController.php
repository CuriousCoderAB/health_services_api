<?php

namespace App\Http\Controllers\Products;

use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function __construct()
    {
        $this->middleware(['admin'])->except(['index', 'show']);
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($type_id)
    {
        $products = Product::where('type_id', '=', $type_id)->get();
        return response()->json(['products' => $products], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'type_id' => ['required', 'exists:product_types,id'],
            'name' => ['required', 'min:2', 'max:50'],
            'description' => ['required', 'min:2', 'max:250'],
            'is_available' => ['required', 'boolean']
        ]);

        Product::create($request->all());

        return response()->json(['message', 'Product created!'], 201);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $product = Product::where('id', $id)->get();
        return response()->json(['product' => $product], 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id)
    {
        request()->validate([
            'type_id' => ['required', 'exists:product_types,id'],
            'name' => ['required', 'min:2', 'max:50'],
            'description' => ['required', 'min:2', 'max:250'],
            'is_available' => ['required', 'boolean']
        ]);

        Product::find($id)->update([
            'type_id' => request()->type_id,
            'name' => request()->name,
            'description' => request()->description,
            'is_available' => request()->is_available
        ]);
        return response()->json(['message', 'Product updated!'], 202);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        Product::destroy($id);
        return response()->json('Product deleted!', 202);
    }
}
