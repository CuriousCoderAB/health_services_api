<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;

class PassiveCheckController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json(['phones' => 'hello?!'], 200);
    }

    /**
     * @param $email
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($email)
    {
        return response()->json(['is_taken' => User::where('email', '=', $email)->exists()]);
    }
}
