<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '';


    /**
     * LoginController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest')->only(['login']);
        $this->middleware('auth:api')->only(['logout']);
    }

    /**
     * @return \Illuminate\Http\JsonResponse|\Psr\Http\Message\StreamInterface
     */
    public function login ()
    {
        $guzzle = new \GuzzleHttp\Client;

        try {
            $response = $guzzle->post(config('services.passport.login_endpoint'), [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => config('services.passport.client_id'),
                    'client_secret' => config('services.passport.client_secret'),
                    'username' => request()->username,
                    'password' => request()->password
                ],
            ]);

            return $response->getBody();

        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            if ($e->getCode() === 400) {
                return response()->json('Invalid Request, Not Found', $e->getCode());
            } elseif ($e->getCode() === 401) {
                return response()->json('Invalid Request, Incorrect Values', $e->getCode());
            }
            return response()->json('Something went wrong.', $e->getCode());
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->user()->tokens->each(function ($token, $key) {
            $token->delete();
        });

        return response()->json('logged out successfully', 200);
    }
}
