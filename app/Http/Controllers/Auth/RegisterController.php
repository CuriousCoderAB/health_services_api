<?php

namespace App\Http\Controllers\Auth;

use App\Mail\UserConfirmEmail;
use App\User;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '';


    /**
     * RegisterController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest')->only(['register']);
        $this->middleware('auth:api')->only(['confirm', 'resendConfirm']);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function register()
    {
        request()->validate([
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'name' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $user = User::create([
            'email' => request()->email,
            'confirmation_token' => substr(md5(now()), 0, 25),
            'confirmation_token_expiry' => Carbon::now()->addDays(3)->toDateString(),
            'name' => request()->name,
            'password' => Hash::make(request()->password),
        ]);

        Mail::to($user)->send(new UserConfirmEmail($user));
        return response()->json('Signed Up', 201);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function confirm()
    {
        request()->validate([
            'confirmation_token' => ['required', 'string']
        ]);

        $confirmation_token = request()->confirmation_token;
        $user = auth()->user();

        if ($user->confirmation_token !== $confirmation_token || auth()->user()->confirmationTokenHasExpired()) {
            return response()->json('This is an invalid token', 400);
        }

        $user->confirm();

        return response()->json(['message' => 'User is confirmed'], 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function resendConfirm()
    {
        $user = request()->user();

        $user->setConfirmationToken();

        Mail::to(request()->user())->send(new UserConfirmEmail(request()->user()));
        return response()->json('Email re-sent', 200);
    }
}
