<?php

namespace App\Http\Controllers\Admins;

use App\Mail\UserConfirmEmail;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware(['admin']);
    }

    public function index()
    {
        return response()->json(User::all());
    }

    public function show($email)
    {
        return response()->json(['user' => User::where('email', $email)->firstOrFail()]);
    }

    public function destroy($email)
    {
        $user = response()->json(User::where('email', $email)->firstOrFail());
        $user->delete();

        return response()->json('User was deleted.');
    }
}
