<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    /**
     * ImagesController constructor.
     */
    public function __construct()
    {
        $this->middleware(['admin']);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function store()
    {
        request()->validate([
            'image' => ['required', 'image', 'mimes:jpg,jpeg,bmp,png', 'max:2068'],
            'caption' => ['nullable', 'string', 'max:255'],
            'reference' => ['nullable', 'string', 'max:255'],
            'description' => ['nullable', 'string', 'max:255'],
        ]);

        $meta = [
            'caption' => request()->caption,
            'reference' => request()->reference,
            'description' => request()->description,
            'main' => true
        ];

        request()->user()->storeImage(request()->file('image'), $meta, true);

        return response()->json('Image added.', 201);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        auth()->user()->Images->find($id)->delete();
        return response()->json('Image deleted', 200);
    }
}
