<?php

namespace App\Http\Controllers\Admins\Notes;

use App\User;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware(['admin']);
    }

    public function index()
    {
        return response()->json(User::all());
    }

    public function show($email)
    {
        return response()->json(['user' => User::where('email', $email)->firstOrFail()]);
    }

    public function destroy($email)
    {
        $user = response()->json(User::where('email', $email)->firstOrFail());
        $user->delete();

        return response()->json('User was deleted.');
    }
}
