<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;

class FavoritesController extends Controller
{

    /**
     * FavoritesController constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth:api', 'mustVerify']);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $products = request()->user()->FavoritedProducts()->get();
        return response()->json(['products' => $products], 200);
    }
}
