<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class PhoneNumbersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth:api', 'mustVerify']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['phones' => request()->user()->Phones], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        request()->validate([
            'type' => ['required', Rule::in(['home', 'work', 'cell'])],
            'country' => ['required', 'size:2'],
            'number' => ['required', 'digits:10'],
            'extension' => ['digits_between:1,10']
        ]);

        request()->user()->Phones()->create([
            'type' => request()->type,
            'country' => request()->country,
            'number' => request()->number,
            'extension' => request()->extension,
        ]);

        return response()->json('Phone number added', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PhoneNumber  $phone
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $phone = auth()->user()->Phones()->findOrFail($id);
        return response()->json(['phone' => $phone], 200);
    }

    public function update()
    {
        request()->validate([
            'type' => ['required', Rule::in(['home', 'work', 'cell'])],
            'country' => ['required', 'size:2'],
            'number' => ['required', 'digits:10'],
            'extension' => ['digits_between:1,10']
        ]);

        $phone = request()->user()->Phones()->where(['id' => request()->phone_id]);

        $phone->update([
            'type' => request()->type,
            'country' => request()->country,
            'number' => request()->number,
            'extension' => request()->extension,
        ]);

        return response()->json('Phone number updated', 202);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $phone = auth()->user()->Phones()->find($id);

        if ($phone) {
            $phone->delete();
            return response()->json('Phone number deleted');
        }
        return response()->json('Phone number not found');
    }
}
