<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AddressesController extends Controller
{

    /**
     * AddressesController constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth:api', 'mustVerify']);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json(['Addresses' =>request()->user()->Addresses], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'street_address' => ['required', 'min:2', 'max:255'],
            'unit_information' => ['max:255'],
            'city' => ['required', 'max:50'],
            'province' => ['required', 'size:2'],
            'country' => ['required', 'max:25'],
            'postal_code' => ['required', 'size:6']
        ]);

        $request->user()->Addresses()->create([
            'street_address' => $request->street_address,
            'unit_information' => $request->unit_information,
            'city' => $request->city,
            'province' => $request->province,
            'country' => $request->country,
            'postal_code' => strtoupper($request->postal_code),
        ]);

        return response()->json('Address added', 201);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $address = auth()->user()->Addresses()->findOrFail($id);
        return response()->json(['address' => $address], 200);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'street_address' => ['required', 'min:2', 'max:255'],
            'unit_information' => ['max:255'],
            'city' => ['required', 'max:50'],
            'province' => ['required', 'size:2'],
            'country' => ['required', 'max:25'],
            'postal_code' => ['required', 'size:6']
        ]);

        $address = $request->user()->Addresses()->find($id);

        $address->update([
            'street_address' => $request->street_address,
            'unit_information' => $request->unit_information,
            'city' => $request->city,
            'province' => $request->province,
            'country' => $request->country,
            'postal_code' => strtoupper($request->postal_code),
        ]);

        return response()->json('Address updated', 202);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $address = auth()->user()->Addresses()->find($id);

        if ($address) {
            $address->delete();
            return response()->json('Address deleted');
        }
        return response()->json('No address found');
    }
}
