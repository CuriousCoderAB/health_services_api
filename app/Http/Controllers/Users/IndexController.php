<?php

namespace App\Http\Controllers\Users;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * IndexController constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth:api', 'mustVerify'])->except(['show']);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json(['user' => auth()->user()]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function show()
    {
        return response()->json(['user' => auth()->user()]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function update()
    {
        request()->validate([
            'name' => ['required', 'min:2']
        ]);

        $user = request()->user();

        $user->update([
            'name' => request()->name
        ]);

        return response()->json('User updated successfully', 202);
    }
}
