<?php

namespace App\Http\Middleware;

use Closure;

class MustVerify
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();

        if (!$user->email_verified_at) {
            abort(403, 'Please check your emails for a verification link or have one resent.');
        }

        return $next($request);
    }
}
