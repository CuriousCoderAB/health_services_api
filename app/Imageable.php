<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 2019-05-05
 * Time: 8:03 PM
 */

namespace App;


use Illuminate\Support\Facades\Storage;

trait Imageable
{
    public function Images()
    {
        return $this->morphMany(Image::class, 'imageable')->latest();
    }

    public function storeImage($image, Array $imageMeta, $isPublic = false)
    {
        $extension = $image->getClientOriginalExtension();
        $name = substr(md5(time()), 0, 20) . ".$extension";
        $visibility = $isPublic ? 'public' : 'private';
        $path =  "$this->storageDirectory/$name";

        if ($imageMeta['main']) {
            foreach ($this->Images() as $image) {
                $image->is_main = false;
            }
        }

        $this->Images()->create([
            'caption' => $imageMeta['caption'] ?: null,
            'reference' => $imageMeta['reference'] ?: null,
            'description' => $imageMeta['description'] ?: null,
            'is_main' => $imageMeta['main'] ?: false,
            'is_visible' => $isPublic,
            'path' => $path,
            'type' => $extension
        ]);

        Storage::put($path, $image, $visibility);
    }
}
