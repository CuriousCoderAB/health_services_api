<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 2019-05-05
 * Time: 8:17 PM
 */

namespace App;


trait Noteable
{
    public function Notes()
    {
        return $this->morphMany(Note::class, 'noteable');
    }
}