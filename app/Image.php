<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Image extends Model
{
    use softDeletes,
        Noteable;

    protected $fillable = [
        'caption',
        'reference',
        'description',
        'is_main',
        'is_visible',
        'path',
        'type',
        'imageable_id',
        'imageable_type',
    ];

    protected $casts = [
        'is_main' => 'boolean',
        'is_visible' => 'boolean'
    ];

    public function imagable()
    {
        return $this->morphTo();
    }
}
