<?php

namespace App;

use App\Scopes\AvailableScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductType extends Model
{
    use softDeletes,
        Imageable,
        Noteable;

    protected $fillable = [
        'name',
        'description',
        'is_available',
        'image'
    ];

    protected $casts = [
        'is_available' => 'boolean',
        'is_service' => 'boolean'
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new AvailableScope);
    }

    public function Products()
    {
        return $this->hasMany(Product::class);
    }
}
