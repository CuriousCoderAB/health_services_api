<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth',
    'namespace' => 'Auth',
], function () {
    Route::get('/email/{email}', 'PassiveCheckController@show')->name('check.email');
    Route::post('/login', 'LoginController@login')->name('login');
    Route::post('/logout', 'LoginController@logout')->name('logout');
    Route::post('/register', 'RegisterController@register')->name('register');
    Route::post('/confirm', 'RegisterController@confirm')->name('confirm');
    Route::post('/resend-confirm', 'RegisterController@resendConfirm')->name('resendConfirm');
});

Route::group([
    'prefix' => 'user',
    'namespace' => 'Users',
    'middleware' => ['auth:api']
], function () {

    Route::get('/', 'IndexController@show')->name('user.show');
    Route::put('/', 'IndexController@update')->name('user.update');

    Route::get('/favorites', 'FavoritesController@index')->name('user.favorites');

    Route::group([
        'prefix' => 'phone'
    ], function () {
        Route::get('/', 'PhoneNumbersController@index')->name('user.phone.index');
        Route::post('/', 'PhoneNumbersController@store')->name('user.phone.store');
        Route::get('/{id}', 'PhoneNumbersController@show')->name('user.phone.show');
        Route::put('/{id}', 'PhoneNumbersController@update')->name('user.phone.update');
        Route::delete('/{id}', 'PhoneNumbersController@destroy')->name('user.phone.destroy');
    });
    Route::group([
        'prefix' => 'address'
    ], function () {
        Route::get('/', 'AddressesController@index')->name('user.address.index');
        Route::post('/', 'AddressesController@store')->name('user.address.store');
        Route::get('/{id}', 'AddressesController@show')->name('user.address.show');
        Route::put('/{id}', 'AddressesController@update')->name('user.address.update');
        Route::delete('/{id}', 'AddressesController@destroy')->name('user.address.destroy');
    });
    Route::group([
        'prefix' => 'image'
    ], function () {
        Route::get('/', 'ImagesController@index')->name('user.image.index');
        Route::post('/', 'ImagesController@store')->name('user.image.store');
        Route::get('/{id}', 'ImagesController@show')->name('user.image.show');
        Route::put('/{id}', 'ImagesController@update')->name('user.image.update');
        Route::delete('/{id}', 'ImagesController@destroy')->name('user.image.destroy');
    });
});

Route::group([
    'prefix' => 'product',
    'namespace' => 'Products'
], function () {
    Route::group([
        'prefix' => 'type',
    ], function () {
        Route::get('/', 'TypesController@index')->name('product.type.index');
        Route::post('/', 'TypesController@store')->name('product.type.store');
        Route::get('/{id}', 'TypesController@show')->name('product.type.show');
        Route::put('/{id}', 'TypesController@update')->name('product.type.update');
        Route::delete('/{id}', 'TypesController@destroy')->name('product.type.destroy');
    });
    Route::group([
        'prefix' => 'favorite',
    ], function () {
        Route::post('/{product}', 'FavoritesController@store')->name('product.type.create');
        Route::delete('/{product}', 'FavoritesController@destroy')->name('product.type.destroy');
    });
    Route::get('/{type_id}', 'IndexController@index')->name('product.index');
    Route::post('/', 'IndexController@store')->name('product.store');
    Route::get('/{id}', 'IndexController@show')->name('product.show');
    Route::put('/{id}', 'IndexController@update')->name('product.update');
    Route::delete('/{id}', 'IndexController@destroy')->name('product.destroy');
});

Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admins'
], function () {
    Route::group([
        'prefix' => 'user',
    ], function () {
        Route::get('/', 'UsersController@index')->name('admin.user.index');
        Route::get('/{id}', 'UsersController@show')->name('admin.user.show');
        Route::put('/{id}', 'UsersController@update')->name('admin.user.update');
        Route::delete('/{id}', 'UsersController@destroy')->name('admin.user.destroy');
    });
});
