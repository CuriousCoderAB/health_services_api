# LaravelForum

## Prerequisites
- PHP ^7.2
- SMTP Credentials (Mailhog is ready, if you use Laravel Homestead or Valet you are ready to go. If you something else, such as MailTrap, you must enter the required information.)
- add `backend.healthservices.com` to your local hosts file.

## Recomendations
If you are looking for a vagrant box to use this application locally I suggest `Homestead`. Follow the instructions in the link below to get started.

https://laravel.com/docs/6.0/homestead

## Installation
```
# Clone project locally
$ git clone git@bitbucket.org:CuriousCoderAB/health_services_api.git

# Go into the project file and install the packages.
$ cd health_services_api && composer install && npm install

# Run the install command
$ php artisan app:setup
```

#### You must do one thing now, look for the following output:
```
Password grant client created successfully.
Client ID: 2
Client secret: bFgkyhXD45gl9X71YfAfC70GXxXeoElXKE1NAApa
```

Copy these values to the `.env` variables:
```
PASSPORT_CLIENT_ID={Client ID}
PASSPORT_CLIENT_SECRET={Client secret}
```
