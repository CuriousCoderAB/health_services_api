<?php

namespace Tests;

use App\Exceptions\Handler;
use App\Product;
use App\ProductType;
use App\User;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Schema;
use Laravel\Passport\Passport;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function setUp(): void
    {
        parent::setUp();
        Schema::enableForeignKeyConstraints();
        $this->disableExceptionHandling();
    }

    protected function signInAdmin(User $admin = null)
    {
        $admin = $admin ?: factory(User::class)->create(['is_admin' => true]);
        Passport::actingAs(
            $admin
        );
        return $this;
    }

    protected function signInUser(User $user = null)
    {
        $user = $user ?: factory(User::class)->create();
        Passport::actingAs(
            $user
        );
        return $this;
    }

    protected function createService($amount = 1, $targetProductType = null)
    {
        $productType = $targetProductType ?: factory(ProductType::class)->create(['is_service' => true]);
        return factory(Product::class, $amount)->create(['product_type_id' => $productType->id]);
    }

    function create($class, $attributes = [], $times = null)
    {
        return factory($class, $times)->create($attributes);
    }
    function make($class, $attributes = [], $times = null)
    {
        return factory($class, $times)->make($attributes);
    }

    protected function disableExceptionHandling()
    {
        $this->oldExceptionHandler = $this->app->make(ExceptionHandler::class);
        $this->app->instance(ExceptionHandler::class, new class extends Handler
        {
            public function __construct()
            {
            }

            public function report(\Exception $e)
            {
            }

            public function render($request, \Exception $e)
            {
                throw $e;
            }
        });
    }

    protected function withExceptionHandling()
    {
        $this->app->instance(ExceptionHandler::class, $this->oldExceptionHandler);
        return $this;
    }
}
