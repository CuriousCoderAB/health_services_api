<?php

namespace Tests\Feature\User;

use App\Product;
use App\ProductType;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductsTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->withExceptionHandling();
        $this->signInUser();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testAUserCannotAddAProductType()
    {
        $newProductType = $this->make(ProductType::class);

        $this->assertCount(0, ProductType::all());

        try {
            $this->post(route('product.type.store'), $newProductType->toArray());
        } catch (\Exception $e) {
            $this->fail('You do not have permission to perform this action.');
        }

        $this->assertCount(0, ProductType::all());
        $this->assertDatabaseMissing('product_types', $newProductType->toArray());
    }

    public function testAUserCannotUpdateAProductType()
    {
        $oldProductType = $this->create(ProductType::class);
        $newProductTypeValues = $this->make(ProductType::class);

        $this->assertCount(1, ProductType::all());

        try {
            $this->put(route('product.type.update', $oldProductType->id), $newProductTypeValues->toArray());
        } catch (\Exception $e) {
            $this->fail('You do not have permission to perform this action.');
        }

        $updatedProductType = ProductType::find($oldProductType->id);

        $this->assertCount(1, ProductType::all());
        $this->assertNotEquals($newProductTypeValues->name, $updatedProductType->name);
        $this->assertNotEquals($newProductTypeValues->description, $updatedProductType->description);

        $this->assertDatabaseHas('product_types', [
            'id' => $oldProductType->id,
            'name' => $oldProductType->name,
            'description' => $oldProductType->description
        ]);
        $this->assertDatabaseMissing('product_types', [
            'name' => $newProductTypeValues->name,
            'description' => $newProductTypeValues->description
        ]);
    }

    public function testAUserCannotDeleteAProductType()
    {
        $targetProductType = $this->create(ProductType::class);

        $this->assertDatabaseHas('product_types', $targetProductType->toArray());

        try {
            $this->delete(route('product.type.destroy', $targetProductType->id));
        } catch (\Exception $e) {
            $this->fail('You do not have permission to perform this action.');
        }

        $updatedProductType = ProductType::find($targetProductType->id);

        $this->assertNull($updatedProductType->deleted_at);
        $this->assertCount(1, ProductType::all());
        $this->assertDatabaseHas('product_types', $targetProductType->toArray());
    }

    public function testAUserCannotAddAProduct()
    {
        $newProduct = $this->make(Product::class);

        $this->assertCount(0, Product::all());

        try {
            $this->post(route('product.store'), $newProduct->toArray());
        } catch (\Exception $e) {
            $this->fail('You do not have permission to perform this action.');
        }

        $this->assertCount(0, Product::all());
        $this->assertDatabaseMissing('products', $newProduct->toArray());
    }

    public function testAUserCannotUpdateAProduct()
    {
        $oldProduct = $this->create(Product::class);
        $newProductValues = $this->make(Product::class);

        $this->assertCount(1, Product::all());

        try {
            $this->put(route('product.update', $oldProduct->id), $newProductValues->toArray());
        } catch (\Exception $e) {
            $this->fail('You do not have permission to perform this action.');
        }

        $updatedProduct = Product::find($oldProduct->id);

        $this->assertCount(1, Product::all());
        $this->assertNotEquals($newProductValues->type_id, $updatedProduct->type_id);
        $this->assertNotEquals($newProductValues->name, $updatedProduct->name);
        $this->assertNotEquals($newProductValues->description, $updatedProduct->description);

        $this->assertDatabaseHas('products', [
            'id' => $oldProduct->id,
            'type_id' => $oldProduct->type_id,
            'name' => $oldProduct->name,
            'description' => $oldProduct->description
        ]);
        $this->assertDatabaseMissing('products', [
            'type_id' => $newProductValues->type_id,
            'name' => $newProductValues->name,
            'description' => $newProductValues->description
        ]);
    }

    public function testAUserCannotDeleteAProduct()
    {
        $targetProduct = $this->create(Product::class);

        $this->assertDatabaseHas('products', $targetProduct->toArray());

        try {
            $this->delete(route('product.destroy', $targetProduct->id));
        } catch (\Exception $e) {
            $this->fail('You do not have permission to perform this action.');
        }

        $updatedProduct = Product::find($targetProduct->id);

        $this->assertNull($updatedProduct->deleted_at);
        $this->assertCount(1, Product::all());
        $this->assertDatabaseHas('products', $targetProduct->toArray());
    }

    public function testAUserCanFavoriteAProduct()
    {
        $targetProduct = $this->create(Product::class);

        $this->assertDatabaseHas('products', $targetProduct->toArray());

        try {
            $this->delete(route('product.destroy', $targetProduct->id));
        } catch (\Exception $e) {
            $this->fail('You do not have permission to perform this action.');
        }

        $updatedProduct = Product::find($targetProduct->id);

        $this->assertNull($updatedProduct->deleted_at);
        $this->assertCount(1, Product::all());
        $this->assertDatabaseHas('products', $targetProduct->toArray());
    }
}
