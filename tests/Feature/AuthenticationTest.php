<?php

namespace Tests\Feature;

use App\User;
use Carbon\Carbon;
use Laravel\Passport\Passport;
use Tests\TestCase;
use App\Mail\UserConfirmEmail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

class AuthenticationTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        Mail::fake();
    }

    /** @test */
    public function testRealMailIsNotSent()
    {
        Mail::assertNothingSent();
    }

    /** @test */
    public function testAUserCanRegisterAnAccount()
    {
        $response = $this->post(route('register'), [
            'email' => 'andrew@example.com',
            'name' => 'Andrew Bilenduke',
            'password' => 'secret88',
            'password_confirmation' => 'secret88',
        ]);
        $response->assertStatus(201);

        $this->assertCount(1, User::all());

        tap(User::first(), function ($user) {
            $this->assertEquals('andrew@example.com', $user->email);
            $this->assertEquals('Andrew Bilenduke', $user->name);
            $this->assertTrue(Hash::check('secret88', $user->password));
        });
    }

    /** @test */
    public function testAConfirmationEmailIsSentUponRegistration()
    {
        $this->post(route('register'), $this->validRegistrationParams());
        Mail::assertQueued(UserConfirmEmail::class);
    }

    /** @test */
    public function testAUserCanConfirmTheirEmailAddress()
    {
        $user = factory(User::class)->create([
            'email_verified_at' => null,
            'confirmation_token' => substr(md5(now()), 0, 25)
        ]);

        Passport::actingAs(
            $user
        );

        $this->assertNull($user->email_verified_at);
        $this->assertNotNull($user->confirmation_token);

        $response = $this->post(route('confirm', ['confirmation_token' => $user->confirmation_token]));
        $response->assertStatus(200);

        tap($user->fresh(), function ($user) {
            $this->assertNotNull($user->email_verified_at);
            $this->assertNull($user->confirmation_token);
        });
    }

    /** @test */
    public function testAnInvalidTokenIsNotAccepted()
    {
        $user = factory(User::class)->create([
            'email_verified_at' => null,
            'confirmation_token' => substr(md5(now()), 0, 25)
        ]);

        Passport::actingAs(
            $user
        );

        $this->assertNull($user->email_verified_at);
        $this->assertNotNull($user->confirmation_token);

        $response = $this->post(route('confirm', ['confirmation_token' => $user->confirmation_token . '88']));
        $response->assertStatus(400);
    }

    /** @test */
    public function testAnExpiredConfirmationTokenIsNotAccepted()
    {
        $user = factory(User::class)->create([
            'email_verified_at' => null,
            'confirmation_token' => substr(md5(now()), 0, 25)
        ]);

        Passport::actingAs(
            $user
        );

        $this->assertNull($user->email_verified_at);
        $this->assertNotNull($user->confirmation_token);

        $user->expireToken();

        $response = $this->post(route('confirm', ['confirmation_token' => $user->confirmation_token]));
        $response->assertStatus(400);
    }

    /** @test */
    public function testAPasswordMustBeEightCharacters()
    {
        $this->withExceptionHandling();
        $this->from(route('register'));

        $response = $this->post(route('register'), $this->validRegistrationParams([
            'password' => 'foo',
            'password_confirmation' => 'foo',
        ]));
        $response->assertSessionHasErrors('password');

        $this->assertFalse(Auth::check());
        $this->assertCount(0, User::all());
    }

    private function validRegistrationParams($overrides = [])
    {
        return array_merge([
            'email' => 'andrew@example.com',
            'name' => 'Andrew Bilenduke',
            'password' => 'secret88',
            'password_confirmation' => 'secret88',
        ], $overrides);
    }
}
