<?php

namespace Tests\Feature\Admin;

use App\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->signInAdmin();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testAnAdminCanAddAnAddressesForAUser()
    {
        $user = $this->create(User::class);

        $response = $this->post(route('admin.user.address.store'), [
            'street_address' => '123 Rue la Faker',
            'city' => 'Ville de Fakers',
            'province' => 'QC',
            'country' => 'Canada',
            'postal_code' => 'a1a1a1'
        ]);

        $response->assertStatus(201);

        $user = User::find($user->id);

        $this->assertTrue($user->addresses->contains('street_address', '123 Rue la Faker'));
        $this->assertTrue($user->addresses->contains('city', 'Ville de Fakers'));
        $this->assertTrue($user->addresses->contains('province', 'QC'));
        $this->assertTrue($user->addresses->contains('country', 'Canada'));
        $this->assertTrue($user->addresses->contains('postal_code', 'A1A1A1'));
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testAnAdminCanAddMultipleAddressesForAUser()
    {
        $user = $this->create(User::class);

        $response = $this->post(route('admin.user.address.store'), [
            'street_address' => '123 Rue la Faker',
            'city' => 'Ville de Fakers',
            'province' => 'QC',
            'country' => 'Canada',
            'postal_code' => 'a1a1a1'
        ]);

        $response->assertStatus(201);

        $response = $this->post(route('admin.user.address.store'), [
            'street_address' => '123 Fakers Crescent',
            'unit_information' => 'apt. 456',
            'city' => 'Fakerstown',
            'province' => 'ON',
            'country' => 'Canada',
            'postal_code' => 'b2b2b2'
        ]);

        $response->assertStatus(201);

        $response = $this->post(route('admin.user.address.store'), [
            'street_address' => '123 Fake Avenue',
            'city' => 'Fakersville',
            'province' => 'AB',
            'country' => 'Canada',
            'postal_code' => 'c3c3c3'
        ]);

        $response->assertStatus(201);

        $user = User::find($user->id);

        $this->assertTrue($user->addresses->contains('street_address', '123 Rue la Faker'));
        $this->assertTrue($user->addresses->contains('city', 'Ville de Fakers'));
        $this->assertTrue($user->addresses->contains('province', 'QC'));
        $this->assertTrue($user->addresses->contains('country', 'Canada'));
        $this->assertTrue($user->addresses->contains('postal_code', 'A1A1A1'));

        $this->assertTrue($user->addresses->contains('street_address', '123 Fakers Crescent'));
        $this->assertTrue($user->addresses->contains('unit_information', 'apt. 456'));
        $this->assertTrue($user->addresses->contains('city', 'Fakerstown'));
        $this->assertTrue($user->addresses->contains('province', 'ON'));
        $this->assertTrue($user->addresses->contains('country', 'Canada'));
        $this->assertTrue($user->addresses->contains('postal_code', 'B2B2B2'));

        $this->assertTrue($user->addresses->contains('street_address', '123 Fake Avenue'));
        $this->assertTrue($user->addresses->contains('city', 'Fakersville'));
        $this->assertTrue($user->addresses->contains('province', 'AB'));
        $this->assertTrue($user->addresses->contains('country', 'Canada'));
        $this->assertTrue($user->addresses->contains('postal_code', 'C3C3C3'));
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testAnAdminCanAddAPhoneNumberForAUser()
    {
        $user = $this->create(User::class);

        $response = $this->post(route('admin.user.phone.store'), [
            'type' => 'home',
            'country' => 'CA',
            'number' => '5555555555',
            'extension' => '8888'
        ]);

        $response->assertStatus(201);

        $user = User::find($user->id);

        $this->assertTrue($user->phones->contains('type', 'home'));
        $this->assertTrue($user->phones->contains('country', 'CA'));
        $this->assertTrue($user->phones->contains('number', '5555555555'));
        $this->assertTrue($user->phones->contains('extension', '8888'));
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testAnAdminCanAddMultiplePhoneNumbersForAUser()
    {
        $user = $this->create(User::class);

        $response = $this->post(route('admin.user.phone.store'), [
            'type' => 'home',
            'country' => 'CA',
            'number' => '5555555555'
        ]);

        $response->assertStatus(201);

        $response = $this->post(route('admin.user.phone.store'), [
            'type' => 'cell',
            'country' => 'CA',
            'number' => '5555555556'
        ]);

        $response->assertStatus(201);

        $response = $this->post(route('admin.user.phone.store'), [
            'type' => 'work',
            'country' => 'CA',
            'number' => '5555555557',
            'extension' => '8888'
        ]);

        $response->assertStatus(201);

        $user = User::find($user->id);

        $this->assertTrue($user->phones->contains('type', 'home'));
        $this->assertTrue($user->phones->contains('country', 'CA'));
        $this->assertTrue($user->phones->contains('number', '5555555555'));

        $this->assertTrue($user->phones->contains('type', 'cell'));
        $this->assertTrue($user->phones->contains('country', 'CA'));
        $this->assertTrue($user->phones->contains('number', '5555555556'));

        $this->assertTrue($user->phones->contains('type', 'work'));
        $this->assertTrue($user->phones->contains('country', 'CA'));
        $this->assertTrue($user->phones->contains('number', '5555555557'));
        $this->assertTrue($user->phones->contains('extension', '8888'));
    }

    public function testAnAdminCanAddAnAvatarForAUser()
    {
        $user = $this->create(User::class);
        $this->signInUser($user);

        $directory = $user->storageDirectory;
        $file = UploadedFile::fake()->image('avatar.jpg');

        Storage::fake($directory);

        $response = $this->post(route('user.image.store'), [
            'image' => $file,
            'caption' => 'The good stuff',
            'reference' => 'andrews images',
            'description' => 'Very good image right here!'
        ]);

        $response->assertStatus(201);

        $savedImage = $user->Images()->first();

        Storage::assertExists($savedImage->path);

        $this->assertEquals('The good stuff', $savedImage->caption);
        $this->assertEquals('andrews images', $savedImage->reference);
        $this->assertEquals('Very good image right here!', $savedImage->description);
        $this->assertTrue($savedImage->is_main);
        $this->assertTrue($savedImage->is_visible);
    }

    public function testAnAdminCanDeleteAnAvatarForAUser()
    {
        $user = $this->create(User::class);
        $this->signInUser($user);

        $directory = $user->storageDirectory;
        $file = UploadedFile::fake()->image('avatar.jpg');

        Storage::fake($directory);

        $response = $this->post(route('admin.user.image.store'), [
            'image' => $file
        ]);

        $response->assertStatus(201);

        $savedImage = $user->Images()->first();

        Storage::assertExists($savedImage->path);

        $response = $this->delete(route('admin.user.image.destroy', $savedImage->id));

        $savedImage = $user->Images()->withTrashed()->first();

        $this->assertNotNull($savedImage->deleted_at);
        $response->assertStatus(200);
    }
}
