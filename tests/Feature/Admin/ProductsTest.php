<?php

namespace Tests\Feature\Admin;

use App\Scopes\AvailableScope;
use App\Product;
use App\ProductType;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductsTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->signInAdmin();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testAnAdministratorCanAddAProductType()
    {
        $newProductType = $this->make(ProductType::class);

        $this->assertEquals(0, ProductType::all()->count());

        $response = $this->post(route('product.type.store'), $newProductType->toArray());
        $response->assertStatus(201);

        $this->assertCount(1, ProductType::all());
        $this->assertDatabaseHas('product_types', $newProductType->toArray());
    }

    public function testAnAdministratorCanUpdateAProductType()
    {
        $oldProductType = $this->create(ProductType::class);
        $newProductTypeValues = $this->make(ProductType::class);

        $this->assertCount(1, ProductType::all());

        $response = $this->put(route('product.type.update', $oldProductType->id), $newProductTypeValues->toArray());
        $response->assertStatus(202);

        $updatedProductType = ProductType::find($oldProductType->id);

        $this->assertCount(1, ProductType::all());
        $this->assertEquals($newProductTypeValues->name, $updatedProductType->name);
        $this->assertEquals($newProductTypeValues->description, $updatedProductType->description);

        $this->assertDatabaseHas('product_types', [
            'id' => $oldProductType->id,
            'name' => $newProductTypeValues->name,
            'description' => $newProductTypeValues->description
        ]);
        $this->assertDatabaseMissing('product_types', [
            'id' => $newProductTypeValues->id,
            'name' => $oldProductType->name,
            'description' => $oldProductType->description
        ]);
    }

    public function testWhenAProductTypeIsMadeUnAvailableAllProductsAreAlsoMadeUnavailable()
    {
        $targetProductType = $this->create(ProductType::class);
        $products = $this->create(Product::class, ['type_id' => $targetProductType->id], 5);

        $response = $this->put(route('product.type.update', $targetProductType->id), [
            'name' => $targetProductType->name,
            'description' => $targetProductType->description,
            'is_available' => false
        ]);
        $response->assertStatus(202);

        $updatedProductType = ProductType::where('id', $targetProductType->id)->get();
        $updatedProducts = Product::where('type_id', $targetProductType->id)->get();

        $this->assertEquals(0, $updatedProductType->count());
        $this->assertEquals(0, $updatedProducts->count());

        $unavailableProductType = ProductType::withoutGlobalScope(AvailableScope::class)->where('id', $targetProductType->id)->get();
        $unavailableProducts = Product::withoutGlobalScope(AvailableScope::class)->where('type_id', $targetProductType->id)->get();

        $this->assertCount(1, $unavailableProductType);
        $this->assertCount(5, $unavailableProducts);

        $this->assertDatabaseHas('product_types', $unavailableProductType->first()->toArray());
        foreach ($unavailableProducts as $product) {
            $this->assertDatabaseHas('products', $product->toArray());
        }
    }

    public function testWhenAProductTypeIsMadeAvailableAllProductsAreAlsoMadeAvailable()
    {
        $targetProductType = $this->create(ProductType::class, ['is_available' => false]);
        $products = $this->create(Product::class, [
            'type_id' => $targetProductType->id,
            'is_available' => false
        ], 5);

        $newProductType = ProductType::where('id', $targetProductType->id)->get();
        $newProducts = Product::where('type_id', $targetProductType->id)->get();

        $this->assertEquals(0, $newProductType->count());
        $this->assertEquals(0, $newProducts->count());

        $unavailableProductType = ProductType::withoutGlobalScope(AvailableScope::class)->where('id', $targetProductType->id)->get();
        $unavailableProducts = Product::withoutGlobalScope(AvailableScope::class)->where('type_id', $targetProductType->id)->get();

        $this->assertCount(1, $unavailableProductType);
        $this->assertCount(5, $unavailableProducts);

        $response = $this->put(route('product.type.update', $targetProductType->id), [
            'name' => $targetProductType->name,
            'description' => $targetProductType->description,
            'is_available' => true
        ]);
        $response->assertStatus(202);

        $updatedProductType = ProductType::where('id', $targetProductType->id)->get();
        $updatedProducts = Product::where('type_id', $targetProductType->id)->get();

        $this->assertCount(1, $updatedProductType);
        $this->assertCount(5, $updatedProducts);

        $this->assertDatabaseHas('product_types', $updatedProductType->first()->toArray());
        foreach ($updatedProducts as $product) {
            $this->assertDatabaseHas('products', $product->toArray());
        }
    }

    public function testAnAdministratorCanDeleteAProductType()
    {
        $targetProductType = $this->create(ProductType::class);

        $this->assertDatabaseHas('product_types', $targetProductType->toArray());

        $response = $this->delete(route('product.type.destroy', $targetProductType->id));
        $response->assertStatus(202);

        $this->assertEquals(0, ProductType::all()->count());
    }

    public function testADeletedProductTypeIsSoftDeleted()
    {
        $targetProductType = $this->create(ProductType::class);

        $this->assertDatabaseHas('product_types', $targetProductType->toArray());

        $response = $this->delete(route('product.type.destroy', $targetProductType->id));
        $response->assertStatus(202);

        $this->assertEquals(0, ProductType::all()->count());

        $this->assertDatabaseHas('product_types', [
            'id' => $targetProductType->id,
            'name' => $targetProductType->name,
            'description' => $targetProductType->description
        ]);
    }

    public function testAnAdministratorCanAddAProduct()
    {
        $newProduct = $this->make(Product::class);

        $this->assertEquals(0, Product::all()->count());

        $response = $this->post(route('product.store'), $newProduct->toArray());
        $response->assertStatus(201);

        $this->assertCount(1, Product::all());
        $this->assertDatabaseHas('products', $newProduct->toArray());
    }

    public function testAnAdministratorCanUpdateAProduct()
    {
        $oldProduct = $this->create(Product::class);
        $newProductValues = $this->make(Product::class);

        $this->assertCount(1, Product::all());

        $response = $this->put(route('product.update', $oldProduct->id), $newProductValues->toArray());
        $response->assertStatus(202);

        $updatedProduct = Product::find($oldProduct->id);

        $this->assertCount(1, Product::all());
        $this->assertEquals($newProductValues->name, $updatedProduct->name);
        $this->assertEquals($newProductValues->description, $updatedProduct->description);

        $this->assertDatabaseHas('products', [
            'id' => $oldProduct->id,
            'type_id' => $newProductValues->type_id,
            'name' => $newProductValues->name,
            'description' => $newProductValues->description
        ]);
        $this->assertDatabaseMissing('products', [
            'id' => $newProductValues->id,
            'type_id' => $oldProduct->type_id,
            'name' => $oldProduct->name,
            'description' => $oldProduct->description
        ]);
    }

    public function testAnAdministratorCanDeleteAProduct()
    {
        $targetProduct = $this->create(Product::class);

        $this->assertDatabaseHas('products', $targetProduct->toArray());

        $response = $this->delete(route('product.destroy', $targetProduct->id));
        $response->assertStatus(202);

        $this->assertEquals(0, Product::all()->count());
    }

    public function testADeletedProductIsSoftDeleted()
    {
        $targetProduct = $this->create(Product::class);

        $this->assertDatabaseHas('products', $targetProduct->toArray());

        $response = $this->delete(route('product.destroy', $targetProduct->id));
        $response->assertStatus(202);

        $this->assertEquals(0, Product::all()->count());

        $this->assertDatabaseHas('products', [
            'id' => $targetProduct->id,
            'type_id' => $targetProduct->type_id,
            'name' => $targetProduct->name,
            'description' => $targetProduct->description
        ]);
    }
}
