<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Image;
use App\Product;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Storage;

$factory->define(Image::class, function (Faker $faker) {
    $image = $faker->image();
    $extension = $image->fileExtension;
    $name = substr(md5(Carbon::now()->toDateTimeString()), 0, 20);
    $path = "/faked/$name.$extension";

    Storage::put($path, $image, 'public');

    return [
        'caption' => $faker->text(50),
        'reference' => $faker->text(50),
        'description' => $faker->text(500),
        'is_main' => true,
        'is_visible' => true,
        'path' => $path,
        'type' => $image->mimeType,
        'imageable_id' => factory(Product::class)->create()->id,
        'imageable_type' => 'App\Product'
    ];
});
