<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Activity;
use App\Product;
use App\User;
use Faker\Generator as Faker;

$factory->define(Activity::class, function (Faker $faker) {
    return [
        'ip_address' => $faker->ipv4,
        'user_id' => factory(User::class, 1)->create()->id,
        'subject_id' => factory(Product::class)->create()->id,
        'subject_type' => 'App\Product',
        'type' => ''
    ];
});
