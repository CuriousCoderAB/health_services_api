<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ProductType;
use Faker\Generator as Faker;

$factory->define(ProductType::class, function (Faker $faker) {
    return [
        'name' => $faker->text(25),
        'description' => $faker->text(),
        'image' => $faker->image(),
        'is_available' => true,
        'is_service' => false
    ];
});
