<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Note;
use App\Product;
use App\User;
use Faker\Generator as Faker;

$factory->define(Note::class, function (Faker $faker) {
    return [
        'creator_id' => factory(User::class)->create()->id,
        'noteable_id' => factory(Product::class)->create()->id,
        'noteable_type' => 'App\Product',
        'title' => $faker->text(25),
        'description' => $faker->text(500),
        'is_task' => false
    ];
});
