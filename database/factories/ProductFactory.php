<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use App\ProductType;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->text(25),
        'description' => $faker->text(),
        'image' => $faker->image(),
        'type_id' => factory(ProductType::class),
        'is_available' => true
    ];
});
