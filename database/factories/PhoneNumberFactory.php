<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\PhoneNumber;
use App\User;
use Faker\Generator as Faker;

$type = [
    'home',
    'work',
    'cell'
];

$factory->define(PhoneNumber::class, function (Faker $faker) use($type) {
    $selectedType = $type[rand(0, 2)];
    return [
        'type' => $selectedType,
        'country' => 'CA',
        'number' => strval(rand(1111111111, 9999999999)),
        'extension' => $selectedType === 'work' ? strval(rand(1, 9999)) : null
    ];
});
