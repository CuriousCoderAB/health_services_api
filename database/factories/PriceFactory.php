<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Price;
use App\Product;
use Faker\Generator as Faker;

$factory->define(Price::class, function (Faker $faker) {
    return [
        'price' => $faker->numberBetween(100, 100000),
        'product_id' => factory(Product::class),
        'is_current' => true,
        'is_sale' => false
    ];
});
