<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Address;
use App\User;
use Faker\Generator as Faker;

$places = [
    ['BC', 'Vancouver'],
    ['ON', 'Toronto'],
    ['QC', 'Montreal']
];

$postalCodes = [
    'A1A1A1',
    'B2B2B2',
    'C3C3C3',
    'D4D4D4'
];

$factory->define(Address::class, function (Faker $faker) use($places, $postalCodes) {

    $selectedPlace = $places[rand(0, 2)];
    $selectedProvince = $selectedPlace[0];
    $selectedCity = $selectedPlace[1];
    $selectedPostalCode = $postalCodes[rand(0, 3)];

    return [
        'street_address' => $faker->streetAddress,
        'unit_information' => rand(0, 1) ? 'Apt. ' . rand(1, 1000) : null,
        'city' => $selectedCity,
        'province' => $selectedProvince,
        'country' => 'Canada',
        'postal_code' => $selectedPostalCode
    ];
});
