<?php

use App\Address;
use App\PhoneNumber;
use App\User;
use App\UserAddress;
use App\UserPhoneNumber;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        User::truncate();
        PhoneNumber::truncate();
        Address::truncate();

        Schema::enableForeignKeyConstraints();

        collect([
            [
                'name' => 'John Doe',
                'email' => 'john@test.com',
                'is_admin' => true
            ],
            [
                'name' => 'Jane Doe',
                'email' => 'jane@test.com',
                'is_admin' => true
            ],
            [
                'name' => 'Ellen Ripley',
                'email' => 'alien@test.com'
            ],
            [
                'name' => 'Tyler Durden',
                'email' => 'everyman@test.com'
            ],
        ])->each(function ($user) {
            $userId = factory(User::class)->create($user)->id;
            factory(Address::class)->create([
                'user_id' => $userId
            ]);
            factory(PhoneNumber::class)->create([
                'user_id' => $userId
            ]);
        });
    }
}
