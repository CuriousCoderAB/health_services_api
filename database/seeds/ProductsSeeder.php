<?php

use App\Price;
use App\Product;
use App\ProductType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        Price::truncate();
        Product::truncate();
        ProductType::truncate();

        Schema::enableForeignKeyConstraints();

        collect([
            [
                'name' => 'Personal Care',
                'description' => 'Services for personal care.',
                'image' => 'types/personal_care.jpg',
                'is_available' => true,
                'is_service' => true
            ],
            [
                'name' => 'Home Diagnostics',
                'description' => 'Home care to determine patient aliments.',
                'image' => 'types/home_diagnostics.jpg',
                'is_available' => true,
                'is_service' => true
            ],
            [
                'name' => 'Disposables',
                'description' => 'Disposable products you will need on a daily basis.',
                'image' => 'types/disposables.jpg',
                'is_available' => true,
                'is_service' => false
            ],
            [
                'name' => 'Equipment',
                'description' => 'High quality, reusable equipment.',
                'image' => 'types/equipment.jpg',
                'is_available' => true,
                'is_service' => false
            ]
        ])->each(function ($ProductType) {
            factory(ProductType::class)->create(
                [
                    'name' => $ProductType['name'],
                    'description' => $ProductType['description'],
                    'image' => $ProductType['image'],
                    'is_available' => $ProductType['is_available'],
                    'is_service' => $ProductType['is_service']
                ]
            );
        });

        collect([
            [
                'name' => 'Nurse Daily Visit',
                'description' => 'A nurse will show up to a patients residence every day.',
                'image' => 'services/nurse_daily_visit.jpg',
                'type_id' => 1,
                'is_available' => true
            ],
            [
                'name' => 'Meal Preparation',
                'description' => 'Three meals daily will be prepared and delivered.',
                'image' => 'services/meal_preparation.jpg',
                'type_id' => 1,
                'is_available' => true
            ],
            [
                'name' => 'Blood Analysis',
                'description' => 'A nurse will come and collect blood samples at the patient\'s residence. (NOTE: The blood will need to be examined at a lab and will take 5 days for results.)',
                'image' => 'services/blood_analysis.jpg',
                'type_id' => 2,
                'is_available' => true
            ],
            [
                'name' => 'Doctor Consultation',
                'description' => 'A doctor will analyze the patient and provide treatments and prescriptions as required.',
                'image' => 'services/doctor_consultation.jpg',
                'type_id' => 2,
                'is_available' => true
            ],
            [
                'name' => 'Face Mask',
                'description' => 'Protective face cover for safe interaction.',
                'image' => 'products/face_mask.jpg',
                'type_id' => 3,
                'is_available' => true
            ],
            [
                'name' => 'Rubber Gloves',
                'description' => 'Disposable gloves. 100 per box',
                'image' => 'products/rubber_gloves.jpg',
                'type_id' => 3,
                'is_available' => true
            ],
            [
                'name' => 'Headwear',
                'description' => 'Disposable headwear. 100 per box',
                'image' => 'products/headwear.jpg',
                'type_id' => 3,
                'is_available' => false
            ],
            [
                'name' => 'Footwear',
                'description' => 'Disposable footwear. 100 per box',
                'image' => 'products/footwear.jpg',
                'type_id' => 3,
                'is_available' => true
            ],
            [
                'name' => 'Wheelchair',
                'description' => 'Aluminum wheel chair, maximum weight 250kgs.',
                'image' => 'products/wheelchair.jpg',
                'type_id' => 4,
                'is_available' => true
            ],
            [
                'name' => 'Crutches',
                'description' => 'Two adjustable crutches for walking assistance.',
                'image' => 'products/crutches.jpg',
                'type_id' => 4,
                'is_available' => true
            ],
        ])->each(function ($Product) {
            factory(Product::class)->create(
                [
                    'name' => $Product['name'],
                    'description' => $Product['description'],
                    'image' => $Product['image'],
                    'type_id' => $Product['type_id'],
                    'is_available' => $Product['is_available']
                ]
            );
        });

        collect([
            [
                'price' => 100000,
                'product_id' => 1,
                'is_current' => true,
                'is_sale' => false
            ],
            [
                'price' => 99000,
                'product_id' => 1,
                'is_current' => true,
                'is_sale' => true
            ],
            [
                'price' => 80000,
                'product_id' => 2,
                'is_current' => true,
                'is_sale' => false
            ],
            [
                'price' => 20000,
                'product_id' => 3,
                'is_current' => true,
                'is_sale' => false
            ],
            [
                'price' => 25000,
                'product_id' => 4,
                'is_current' => true,
                'is_sale' => false
            ],
            [
                'price' => 19000,
                'product_id' => 4,
                'is_current' => true,
                'is_sale' => true
            ],
            [
                'price' => 500,
                'product_id' => 5,
                'is_current' => true,
                'is_sale' => false
            ],
            [
                'price' => 300,
                'product_id' => 6,
                'is_current' => true,
                'is_sale' => false
            ],
            [
                'price' => 700,
                'product_id' => 7,
                'is_current' => true,
                'is_sale' => false
            ],
            [
                'price' => 650,
                'product_id' => 8,
                'is_current' => true,
                'is_sale' => false
            ],
            [
                'price' => 200000,
                'product_id' => 9,
                'is_current' => true,
                'is_sale' => false
            ],
            [
                'price' => 30000,
                'product_id' => 10,
                'is_current' => true,
                'is_sale' => false
            ]
        ])->each(function ($ProductPrice) {
            factory(Price::class)->create(
                [
                    'price' => $ProductPrice['price'],
                    'product_id' => $ProductPrice['product_id'],
                    'is_current' => $ProductPrice['is_current'],
                    'is_sale' => $ProductPrice['is_sale']
                ]
            );
        });
    }
}
